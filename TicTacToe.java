import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);

		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		boolean game = true;
		while (game) {
			printBoard(board);

			while (true) {
				System.out.print("Player 1 enter row number:");
				int row = reader.nextInt();
				System.out.print("Player 1 enter column number:");
				int col = reader.nextInt();
				if (row <= 3 && 1 <= row && col <= 3 && 1 <= col && board[row - 1][col - 1] == ' ') {
					board[row - 1][col - 1] = 'X';
					break;
				} else {
					System.out.println("Please enter a valid number.");
				}
			}

			printBoard(board);
			game = check(board);
			if (!game) {
				break;
			}

			while (true) {
				System.out.print("Player 2 enter row number:");
				int row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				int col = reader.nextInt();
				if (row <= 3 && 1 <= row && col <= 3 && 1 <= col && board[row - 1][col - 1] == ' ') {
					board[row - 1][col - 1] = 'O';
					break;
				} else {
					System.out.println("Please enter a valid number.");
				}
			}

			game = check(board);
		}

		reader.close();
	}

	public static boolean check(char[][] board) {
		for (int i = 0; i < 3; ++i) {
			if (board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] != ' ') {
				System.out.println(board[i][0] + " wins!");
				return false;
			}
		}
		for (int i = 0; i < 3; ++i) {
			if (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] != ' ') {
				System.out.println(board[i][0] + " wins!");
			return false;
			}
		}


		if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ') {
			System.out.println(board[1][1] + " wins!");
			return false;
		} else if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ') {
			System.out.println(board[1][1] + " wins!");
			return false;
		}

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (board[i][j] == ' ') {
					return true;
				}
			}
		}

		System.out.println("It's a tie!");
		return false;
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}